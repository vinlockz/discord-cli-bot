const fs = require('fs');

const {
  BITBUCKET_BUILD_NUMBER,
  IMAGE_NAME,
  ENV_SECRET_NAME,
  DOCKER_REGISTRY_URL,
} = process.env;

const values = {
  BUILD_NUMBER: BITBUCKET_BUILD_NUMBER,
  IMAGE_NAME,
  ENV_SECRET_NAME,
  DOCKER_REGISTRY_URL,
};

fs.readFile('deployment.yml', 'utf8', function (err, data) {
  if (err) return console.error(err);
  let result = data;
  Object.keys(values).forEach(function (value) {
    console.log(`Replacing "{{${value}}}" with "${values[value]}"`);
    result = result.replace(`{{${value}}}`, values[value]);
  });
  fs.writeFile('deployment.yml', result, 'utf8', function (err) {
    if (err) return console.error(err);
    else return console.log('DONE');
  });
});
