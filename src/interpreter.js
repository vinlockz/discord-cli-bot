const interpret = (message) => {
  const interpreted = {};

  // discord create-channel --type="text test" test-channel
  const split = message.content.split(' ');
  console.log(split);
  const fixedSplit = [];

  // Fix Quoted strings
  for (let i = 0; i < split.length; i += 1) {
    let flag = '';
    const hasDoubleQuotes = split[i].includes('"') && !split[i].endsWith('"');
    if (hasDoubleQuotes) {
      do {
        flag = flag.concat((flag !== '') ? ' ' : '', split[i]);
        if (!flag.endsWith('"')) {
          i += 1;
        }
      } while (!flag.endsWith('"'));
    } else {
      flag = split[i];
    }
    fixedSplit.push(flag);
  }

  interpreted.application = fixedSplit[0];
  interpreted.command = fixedSplit[1];
  fixedSplit.shift();
  fixedSplit.shift();

  fixedSplit.forEach((param) => {
    if ((param.startsWith('--') || param.startsWith('-'))  && param.includes('=')) {
      const flag = param.split('=');
      const key = flag[0].substr(param.startsWith('--') ? 2 : 1);
      let value = flag[1];
      if (flag.length > 2) {
        flag.shift();
        value = flag.join('=');
      }
      if (typeof interpreted.flags !== 'object') interpreted.flags = {};
      interpreted.flags[key] = value;
      if (value.startsWith('"') && value.endsWith('"')) {
        interpreted.flags[key] = value.substr(1).slice(0, -1);
      }
    } else if (param.startsWith('--') || param.startsWith('-')) {
      if (typeof interpreted.flags !== 'object') interpreted.flags = {};
      interpreted.flags[param.substr(param.startsWith('--') ? 2 : 1)] = true;
    } else {
      if (!Array.isArray(interpreted.parameters)) {
        interpreted.parameters = [];
      }
      interpreted.parameters.push(param);
    }
  });

  console.log('interpreted', interpreted);

  return interpreted;
};

export default interpret;
