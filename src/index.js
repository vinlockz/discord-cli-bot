import Discord from 'discord.js';
import commands from './commands';
import interpret from './interpreter';
import { codeSnippet, bold } from './utils';
import permissions from './permissions';

const {
  APP_DISCORD_TOKEN,
  APP_CLI_CHANNEL,
  APP_LOG_CHANNEL,
} = process.env;

const client = new Discord.Client();

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('guildMemberAdd', member => {
  // Send the message to a designated channel on a server:
  const channel = member.guild.channels.find(ch => ch.id === APP_LOG_CHANNEL);
  // Do nothing if the channel wasn't found on this server
  if (!channel) {
    console.error(`Channel ${APP_LOG_CHANNEL} not found.`);
    return;
  }
  // Send the message, mentioning the member
  channel.send(codeSnippet(`${member.displayName} - ${member.id} has joined the server.`))
});

client.on('message', msg => {
  if (!msg.author.bot && msg.channel.id === APP_CLI_CHANNEL && msg.content.startsWith('discord')) {
    const interpreted = interpret(msg);
    let allowCommand = false;
    if (Object.prototype.hasOwnProperty.call(permissions, interpreted.command)) {
      const guildMember = msg.guild.members.find(member => msg.author.id === member.id);
      if (guildMember) {
        const allowedRoles = permissions[ interpreted.command ];
        const intersectedRoles = guildMember.roles.filter(role => allowedRoles.includes(role.id));
        if (intersectedRoles.length > 0) {
          allowCommand = true;
        }
      }
    } else {
      allowCommand = true;
    }
    if (!allowCommand) {
      msg.channel.send(`${ msg.author } You do not have permissions for that command.`);
    } else {
      if (interpreted.command === 'help') {
        let help = 'These are the possible commands:\n```\n';
        Object.keys(commands).forEach((command) => {
          if (commands[command].example) {
            help = help.concat(`${ commands[command].example }\n`)
          }
        });
        help = help.concat('```');
        msg.channel.send(help);
      } else {
        Object.keys(commands).forEach((command) => {
          if (commands[ command ].name === interpreted.command) {
            const logChannel = msg.guild.channels.find(ch => ch.id === APP_LOG_CHANNEL);
            const c = new commands[ command ](client, msg, interpreted, logChannel);
            c.run();
          }
        });
      }
    }
  } else if (!msg.author.bot && msg.channel.id === API_CLI_CHANNEL) {
    console.log('msg.content', msg.content);
    msg.channel.send(`${bold('ERROR:')} Invalid Command.`);
  }
});

client.login(APP_DISCORD_TOKEN);
