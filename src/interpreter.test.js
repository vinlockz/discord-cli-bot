import interpret from './interpreter';

describe('Interpreter', () => {
  it('should interpret message properly', () => {
    const message = {
      content: 'discord create-channel --type="text test" test-channel',
    };
    expect(interpret(message)).toEqual({
      application: 'discord',
      command: 'create-channel',
      flags: {
        type: 'text test',
      },
      parameters: [
        'test-channel',
      ],
    });
  });
});