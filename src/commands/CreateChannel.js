import Command from './Command';
import { codeSnippet, bold } from '../utils';
import moment from 'moment';

const {
  APP_LOG_CHANNEL
} = process.env;

let logChannel = null;

const expiringChannels = {};

console.log('Starting cron to check channel expirations...');
setInterval(() => {
  console.log('Running Expiration Check');
  console.log('Channels: ', expiringChannels);
  Object.keys(expiringChannels).forEach((channelId) => {
    if (logChannel === null) {
      logChannel = expiringChannels[ channelId ].channel
        .guild.channels.find(ch => ch.id === APP_LOG_CHANNEL);
    }
    new Promise((res, rej) => {
      const channel = expiringChannels[channelId].channel;
      const expiresAt = expiringChannels[channelId].expiresAt;
      if (expiresAt.isBefore(moment())) {
        delete expiringChannels[channelId];
        channel.delete().then(res).catch(rej);
        console.log('Channel Deleted: ', channelId);
      }
    }).then((channel) => {
      logChannel.send(codeSnippet(`#${channel.name} has expired`));
    }).catch((err) => {
      logChannel.send(codeSnippet(`ERROR: ${err.message}`));
    });
  });
}, 10000);

class CreateChannel extends Command {
  static name = 'create-channel';
  static example = 'discord create-channel {channelName} [--text|voice|category|news|store] [--expires|e={60s|30m|5h|1d}]';

  run = () => {
    // this.msg.channel.send(codeSnippet('test'));
    const channelName = this.params[0];
    let type = 'text';
    if (this.getFlag('voice')) {
      type = 'voice';
    } else if (this.getFlag('category')) {
      type = 'category';
    } else if (this.getFlag('news')) {
      type = 'news';
    } else if (this.getFlag('store')) {
      type = 'store';
    }

    this.msg.guild.createChannel(channelName, { type })
      .then((channel) => {
        let expires = this.getFlag('expires', 'e');
        let expiresAt = false;
        if (expires) {
          if (expires.endsWith('s')) {
            expiresAt = moment().add(Number(expires.slice(0, -1)), 'seconds');
          } else if (expires.endsWith('m')) {
            expiresAt = moment().add(Number(expires.slice(0, -1)), 'minutes');
          } else if (expires.endsWith('h')) {
            expiresAt = moment().add(Number(expires.slice(0, -1)), 'hours');
          } else if (expires.endsWith('d')) {
            expiresAt = moment().add(Number(expires.slice(0, -1)), 'days');
          }
          if (expiresAt) {
            expiringChannels[channel.id] = { channel, expiresAt };
          }
        }

        let message = `${channel} created.`;
        if (expires && expiresAt) {
          message = message.concat(` ${bold('Expires on:')} ${expiresAt.format('LLLL')}`)
        }
        this.channel.send(message);
      });
    this.log(`${channelName} created by ${this.author.username}.`);
  };
}

export default CreateChannel;
