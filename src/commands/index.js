import CreateChannel from './CreateChannel';
import DeleteChannel from './DeleteChannel';

export default {
  CreateChannel,
  DeleteChannel,
};
