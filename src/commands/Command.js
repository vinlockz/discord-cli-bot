import { codeSnippet } from '../utils';

class Command {
  constructor(discord, message, interpreted, logChannel) {
    this.discord = discord;
    this.msg = message;
    this.author = message.author;
    this.guild = message.guild;
    this.channel = message.channel;
    this.flags = interpreted.flags;
    this.params = interpreted.parameters;
    this._logChannel = logChannel;
  }

  getFlag = (flag, shortFlag) => this.flags[flag] || this.flags[shortFlag] || false;

  log = message => this._logChannel.send(codeSnippet(message));
}

export default Command;
