import Command from './Command';
import { bold } from '../utils';

class DeleteChannel extends Command {
  static name = 'delete-channel';
  static example = 'discord delete-channel {channelMention}';

  run = () => {
    let channel = this.params[0];
    if (!channel.startsWith('<#') || !channel.endsWith('>')) {
      this.channel.send(`${bold('ERROR:')} Invalid Channel mention.`);
    } else {
      channel = channel.substr(2).slice(0, -1);
      channel = this.guild.channels.find(ch => ch.id === channel);
      if (!channel) {
        this.channel.send(`${ bold('ERROR:') } Channel not found.`);
      } else {
        channel.delete()
          .then(() => {
            this.channel.send(`#${ channel.name } deleted.`);
            this.log(`${ channel.name } deleted by ${ this.author.username }`);
          });
      }
    }
  };
}

export default DeleteChannel;
