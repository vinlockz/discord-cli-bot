import Command from './Command';
import { bold } from '../utils';

const addableRoles = [
  '583113079039066113', // college
  '583113041013374986', // security engineer
  '583113108554252299', // server-side web engineer
  '583113287672004628', // front-end web engineer
  '583113132017188894', // designer
  '574417614449803293', // seen endgame
];

class AddRole extends Command {
  static name = 'add-role';
  static example = 'discord add-role {roleMention}';

  run = () => {
    let role = this.params[0];
    if (!role.startsWith('<@&') || !channel.endsWith('>')) {
      this.channel.send(`${bold('ERROR:')} Invalid Role mention.`);
    } else {
      role = role.substr(3).slice(0, -1);
      role = this.guild.roles.find(r => r.id === role);
      if (!role) {
        this.channel.send(`${bold('ERROR:')} Role not found.`);
      } else {
        if (addableRoles.includes(role.id)) {
          this.author.addRole(role.id)
            .then(() => {
              this.channel.send(`${this.author} given the role ${role}`);
              this.log(`${this.author.nickname} given the role ${role.name}`);
            });
        } else {
          this.channel.send(`${bold('ERROR:')} Role not allowed.`);
        }
      }
    }
  };
}