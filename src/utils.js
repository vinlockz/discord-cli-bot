export const codeSnippet = message => `\`\`\`${message}\`\`\``;
export const code = message => `\`${message}\``;
export const bold = message => `**${message}**`;
export const italic = message => `*${message}*`;
export const underline = message => `__${message}__`;
